---
title: The Monty Hall problem and the Bayes' theorem
summary: Using the Bayes' theorem to solve the "car and goats" problem 
date: 2023-06-18T10:03:08-04:00
categories:
- math
cover: monty-open-door.png
coverCaption: "[image credit](https://commons.wikimedia.org/wiki/File:Monty_open_door.svg)"
draft: false
showSummary: true
showTableOfContents: false
showComments: true
---
{{< katex >}}

I went through an interesting conversation lately about the [Monty Hall problem](https://en.wikipedia.org/wiki/Monty_Hall_problem). At the end, I thought that this problem was a good use case for the [Bayes' theorem](https://en.wikipedia.org/wiki/Bayes%27_theorem), as solving the problem illustrates the change of probability on a particular event as we gain contextual information about the system.

This posts collects my own notes about solving the Monty Hall problem using the Bayes' theorem, but they tell nothing new about the topic. There is plenty of content about this approach just one web search away.

## The Monty Hall problem

This is a brief summary of this well known problem.

*The setting*:

* There are three closed doors, let's call them **A**, **B**, and **C**. Behind each door there is either a goat or a car, but there is only one car in the ensemble. Let's refer to the door with the car behind it as the "winning door."

* There is a *player* who doesn't know what's behind the doors.

* There is a *game host* who already knows the location of the car, that is, the host knows which door is the winning door.

*The sequence of events*:

* Stage 1: The player picks one door at random, say door **A**. The selected door remains closed.

* Stage 2: The game host selects and opens a different door, say door **C**, showing that there is a goat behind it. The game host never opens the winning door (which could be door **A** or **B**).

* Stage 3: The player is asked to select one door to keep, either door **A**, the player's original selection, or door **B**, which hasn't yet been selected by anyone. Which selection maximizes the chances of selecting the winning door?

*Stage 1* is the starting point for the player, where no prior information about the winning door is available. During this stage all doors are equally probable of being the winning door. *Stage 2* provides the player with an update on the system whereby we get to know that door C is not the winning door. *Stage 3* is where we want to find how the new acquired information from stage 2 alters the probabilities of selecting the winning door.

## Estimating the probabilities

Let's define the following events:

* \\(A, B,\\) and \\(C\\): The car is behind doors **A**, **B**, or **C** respectively.
* \\(H_A, H_B,\\) and \\(H_C\\): The game host opens doors **A**, **B**, or **C** respectively.

During stage 1 the winning probabilities are straight forward, as selecting one door or another makes no difference. That is, we have \\(P(A)=P(B)=P(C)=1/3\\).
During stage 2 the game host opens door **C**, that is, the event \\(H_C\\) takes place.

For stage 3, our target is to evaluate the two conditional probabilities \\(P(A|H_C)\\) and \\(P(B|H_C)\\), and to select the highest one. Using the Bayes' theorem we then want to evaluate the following expressions:

$$P(A|H_C) = \frac{P(H_C|A) \times P(A)}{P(H_C)}$$ and
$$P(B|H_C) = \frac{P(H_C|B) \times P(B)}{P(H_C)}$$

* We already know that \\(P(A)=P(B)=1/3\\)
* \\(P(H_C|A)=1/2\\) because if the car is behind door **A** then the game host selects between doors **B** and **C** with equal probability.
* \\(P(H_C|B)=1\\) because if the car is behind door **B** then the game host selects door **C** with certainty.
* For completeness, \\(P(H_C|C)=0\\) because the game host never opens the winning door.
* We can calculate \\(P(H_C)\\) using the law of total probability: 
  $$
  \begin{align*}
    P(H_C) &= P(H_C|A) \times P(A) + P(H_C|B) \times P(B) + P(H_C|C) \times P(C) \\\
     &= 1/3 \times \left( P(H_C|A) + P(H_C|B) + P(H_C|C) \right) \\\
     &= 1/3 \times (1/2 + 1 + 0) = 1/2
    \end{align*}
  $$

Finally, these are the desired probabilities:

$$P(A|H_C) = \frac{1/2 \times 1/3}{1/2} = \frac{1}{3}$$
and
$$P(B|H_C) = \frac{1 \times 1/3}{1/2} = \frac{2}{3}$$

Switching the selection from door **A** to door **B** therefore gives the player twice the chances of winning the car!

## Intuition
The most common reaction when we get to stage 3 is to reason along this line: "*Now I have two doors to choose from instead of three, my chances are 50/50. Therefore, I can't go wrong selecting either door.*"

To counter this reasoning, the best argument I've found is to think about not three by 10 doors instead (or more).
* At stage 1 the player's chances of selecting the winning door are just 1 in 10.
* At stage 2 the game host opens eight doors, all of them showing goats behind them!
  Two doors remain closed, the one the player selected in stage 1, and "*that other door, hmm* 🤔"
* At stage 3 the player must think "*Wow, the game host, who knows where the winning door is, went through all those eight doors and just left that other door closed for me ... he knows better than my unlikely initial guess, I bet the car is there.*"

The same line of thinking applies to the case of three doors, but it is harder to visualize. Note that at stage 1 there is a 2/3 chance of the car being in either door **B** or **C**.
At stage 3, the thinking could then go like this: "*Now I have two doors to choose from instead of three, but the game host just **collapsed** the 2/3 chance, that was associated with two doors before, into a single door, door **B**. I'll take door **B**!*"

At the end, what matters is that stage 2 provides additional information that the player shouldn't discard, which is what happens if the conclusion were that the probabilities at stage 3 are 50/50.
