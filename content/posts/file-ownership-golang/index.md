---
title: How to Retrieve File Ownership Information in Golang
summary: Working with Linux file attributes in Go
date: 2018-07-07T23:34:21.000Z
cover: golang-files.png
categories:
  - golang
tags:
  - golang
  - files
draft: false
showSummary: true
showTableOfContents: false
---

Figuring out how to retrieve the ownership of a file on my Linux workstation
from within a Go program was not that evident the first time that I
tried. By ownership I mean the `uid` and `gid` values associated with a file. This post
describes how I did it.

Let's assume that the variable `path` holds the name of a file. The entry point
is the [os.stat](https://golang.org/pkg/os/#File.Stat) function which returns a
[FileInfo](https://golang.org/pkg/os/#FileInfo) structure:

```golang
info, err := os.Stat(path)

```

Using the `info` structure above you can invoke the `Sys()` function, which
returns the [Stat](https://golang.org/pkg/syscall/#Stat_t) structure defined in
the `syscall` package:

```golang
stat := info.Sys().(*syscall.Stat_t)
```

The `(*syscall.Stat_t)` type assertion is necessary because the function `Sys()`
is defined to return the empty interface (`interface{}`).

Finally, you can retrieve the file's `uid` and `gid` from the `stat` structure
above as follows:

```golang
uid := stat.Uid
gid := stat.Gid
```

Note that `uid` and `gid` are `uint32` values.

## Getting the User and Group Names

You can retrieve the user and group names using the `os/user` package. First,
you need to covert the `uint32` variables into strings:

```golang
u := strconv.FormatUint(uint64(uid), 10)
g := strconv.FormatUint(uint64(gid), 10)
```

Now you can retrieve the desired `usr` and `group` strings:

```golang
usr, err := user.LookupId(u)
group, err := user.LookupGroupId(g)
```

## A Sample Go Program
A complete Go program implementing the steps above is available from my _Gitlab_
repository; it is called
[ugid](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/ugid). Here is
a sample output:

```bash
$ bin/ugid README.md /etc/magic /dev/mem
README.md: pedro, pedro (1000, 1000)
/etc/magic: root, root (0, 0)
/dev/mem: root, kmem (0, 15)
```
