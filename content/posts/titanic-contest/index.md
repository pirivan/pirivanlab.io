---
title: Participating in the Titanic Contest at Kaggle
summary: An entry-level data analysis exercise
date: 2019-03-25T15:19:40.000Z
cover: titanic.jpg
categories:
  - Data Science
tags:
  - titanic
draft: false
showSummary: true
showTableOfContents: true
---

I wrote three Jupyter notebooks dealing with the Titanic data, as available from
the corresponding [Kaggle](https://www.kaggle.com/c/titanic) web site. I
submitted my survival prediction and I got a score of 0.77033. I am happy with
this score for a first entry.

The notebooks are available for online reading as follows:

1. [Titanic Data Cleanup](https://nbviewer.jupyter.org/urls/gitlab.com/pirivan/dataware/raw/master/pydata/titanic/titanic-1.ipynb).

1. [Titanic Data Exploration](https://nbviewer.jupyter.org/urls/gitlab.com/pirivan/dataware/raw/master/pydata/titanic/titanic-2.ipynb).

1. [Titanic Machine Learning Models](https://nbviewer.jupyter.org/urls/gitlab.com/pirivan/dataware/raw/master/pydata/titanic/titanic-3.ipynb).


The notebooks are also available in source form from my
[GitLab](https://gitlab.com/pirivan/dataware/blob/master/pydata/titanic)
repository.
