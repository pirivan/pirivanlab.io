---
title: Creating a DNS Alias for a DDNS Domain
summary: Or how to use a single DNS domain at home and anywhere else
date: 2018-04-30T23:04:46.000Z
categories:
  - administration
  - networking
tags:
  - DNS
cover: directions.jpg
coverCaption: "[image credit](https://pixabay.com/en/one-way-street-decisions-opportunity-1991865/)"
draft: false
showSummary: true
showTableOfContents: false
---

I recently added the DNS CNAME record `home.colcan.ca` to my `colcan.ca` domain.
The new record is an alias for `colcan.dyndns.info`. A DNS query on the former
looks like this:

```bash
$ host home.colcan.ca 8.8.8.8
Using domain server:
Name: 8.8.8.8
Address: 8.8.8.8#53
Aliases: 

home.colcan.ca is an alias for colcan.dyndns.info.
colcan.dyndns.info has address 70.51.85.201
```

This was interesting for me because `home.colcan.ca` is the internal domain that
I use in my home network. This domain used to work only while connected at home,
it was never public. The domain `colcan.dyndns.info` is the one I chose for my
DDNS service long ago, which I have used to reach my home router when being
outside home. With this new alias I can use now `home.colcan.ca` consistently no
matter where I am.

Why is this good? Well, there are a couple of reasons: First, I don't have to
juggle between the internal and the DDNS names when accessing my home services
(storage, photo album, and so on) anymore depending on my physical location,
which simplifies the configuration of my client applications. Second, it makes
creating SSL certificates simpler because I can use the same SSL certificate for
the `home.colcan.ca` domain on both the internal and the external faces of my
home network.

This last point was really the trigger for me to look into this DNS alias
option. I used the SSL certificates service from [Let’s
Encrypt](https://letsencrypt.org/) to move all HTTP services to HTTPS. Having a
single domain for every service, both inside and outside my home network, made
configuring the SSL certificates much easier.
