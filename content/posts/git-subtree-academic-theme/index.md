---
title: Using git subtree to Install the Academic Theme for Hugo
summary: Or how to use git subtree instead of git submodule
date: 2019-05-05T17:42:47.000Z
cover: green-park.jpg
coverCaption: "[image credit](https://pixabay.com/photos/green-park-season-nature-outdoor-1072828/)"
categories:
  - administration
tags:
  - git
  - academic
draft: false
showSummary: true
showTableOfContents: false
---

This post details how I installed the [Academic
Theme](https://sourcethemes.com/academic) for [Hugo](https://gohugo.io/), which
I use to generate this web site.

There are two differences in the way I create my repository when compared with
the installation steps suggested in the Academic theme website:

1. I do not fork and clone the `academic-kickstart` website, as this
   brings the git history of that project into the history of my website. I want
   to keep my website history clean with just the changes that I make.

1. I use `git subtree` instead of `git submodule` to incorporate the Academic
   theme into my site. Git subtrees make pulling changes from the theme master
   repository easier, which is all I want to do. Git subtrees also complicate
   pushing changes to the theme repository, but this is something I am not
   planning to do.

# Creating the Website
The first step in creating the website is to install the *Hugo* static content
management system. I installed it using the following command:

```bash
sudo snap install hugo
```

Next, create a working directory, a new *Hugo* site, and initialize git:

```bash
mkdir -p ~/Workspace
cd ~/Workspace
hugo new site pirivan.gitlab.io
cd pirivan.gitlab.io
git init
git add .
git commit -am "Initial commit from hugo new site command"
```

# Configuring the Academic Theme
You have to add and set up a theme on the new *Hugo* website to make it
usable. I use the command `git subtree` to incorporate the Academic theme into
the already existing `themes` directory.

```bash
git remote add hugo-academic https://github.com/gcushen/hugo-academic.git
git subtree add --prefix=themes/academic hugo-academic master --squash
```

The `git remote` command creates a local reference to the *hugo-academic* theme
repository. The `git subree` command clones the master branch of this repository
under the `themes/academic` directory while squashing all commits (I am not
interested in the commit details of the upstream repository).

Git knows already about the two repositories, the local master repository and
the remote `hugo-academic` one.

```bash
git branch -a
  * master
  remotes/hugo-academic/master
```

Something good about using `git subtree` is that there is no visible metadata
about them to watch for, like the `.gitmodules` file when you use
`git submodule`. There are however two new commits in the repository:

```bash
$ git log
commit f585fc51c0229d7b3bdcbb8c2c8fcd819844fcb1
Merge: edd4a17 ef7db14

    Merge commit 'ef7db146f3f27106c69803df8cc651dea6b2796d' as 'themes/academic'

commit ef7db146f3f27106c69803df8cc651dea6b2796d

    Squashed 'themes/academic/' content from commit 817cac0
    
    git-subtree-dir: themes/academic
    git-subtree-split: 817cac04ddd40a673342211bf5adfe147a0760ba

commit edd4a17cb6a5c0fcab82070aa04bef4c3a0b74bd

    Initial commit from hugo new site command

```

This first commit records the subtree squashing and merging operations, and the
second one the merge operation into the hosting repository.

# Adding Sample Content
The Academic theme comes with sample website content that you can use as a
starting point. The following commands copy and commit the sample content into
your working directory;

```bash
cp -av themes/academic/exampleSite/* .
git add .
git commit -am "Make site render the default academic theme example content"
```

You can test the website locally using the command `hugo server` and then
visiting `http://localhost:1313` with your browser. If you prefer to use a
non-local address, then you can use a command like the following:

```bash
$ hugo server -D --bind 192.168.1.100 --baseURL http://192.168.1.100
```

The Hugo server updates the test website as soon as you change any of the files
in your working repository. It is time now to start customizing it and adding
your own content. [Getting
Started](https://sourcethemes.com/academic/docs/get-started/) is a good starting
point.

# Updating the Academic Theme

When you want to sync up with newer versions of the Academic theme you have to
update the clone repository under the `themes/academic` directory. You can do
this with the following command:

```bash
git subtree pull --prefix=themes/academic hugo-academic master --squash 
```

This is basically the same command you used before to create the initial
clone. This time you use `git subtree pull` instead of `git subtree add`, but
all other parameters are the same. It is pretty simple indeed.

As before, you will see two commits in the git log tracking the pull and merge
operations. 
