---
title: Golearn
summary: "Golang Learning Path (closed :thumbsup:)"
date: 2019-07-29T14:58:53-04:00
draft: false
showSummary: true
---


This project is my personal path to learning the Go programming language. The
plan includes the following goals:

- &#9745; Completing the [Tour of Go](https://tour.golang.org/) tutorial. My
  solutions to the exercises are available from the
  [snippets](https://gitlab.com/pirivan/golearn/snippets) section in Gitlab.

- &#9745; Reading the [Go System
  Programming](https://www.packtpub.com/networking-and-servers/go-systems-programming)
  book and writing Go programs inspired by the exercises listed at the end of
  each chapter. See the _Changelog_ section below for references to all Go
  programs that I wrote for this goal.

- &#9745; Reading the [Mastering
  Go](https://www.packtpub.com/networking-and-servers/mastering-go) book and
  writing Go programs for each chapter. The objective is to explore advanced Go
  features.

- &#9758; Reading the [Building Microservices with
  Go](https://www.packtpub.com/application-development/building-microservices-go)
  book, and other similar material; write a sample but complete application
  using microservices and deploy it in Google Cloud. This goal is my current
  focus.

Source code for the learning exercises is available from the `master` branch of
my [Golearn](https://gitlab.com/pirivan/golearn) project in Gitlab. The
`development` branch holds work in progress.

## Changelog
- &#9745; [Jul 29, 2019] I finished reading _Mastering Go_. There is not much
  new material when compared with the _Go System Programming_ book and the
  exercises I have developed already. The only exception is chapter 11, _Code
  Testing, Optimization, and Profiling_. In particular, using the Go profiler
  and the Go native cross-compilation capabilities were new topics to me.

- [July 3, 2019] Exercises for chapter 2 of _Mastering Go_ are complete. Chapter
  2 includes an example of passing a Go string to C but not the opposite. I wrote a
  program to retrieve the `uname` information from C and pass it as a string to
  Go. <br/>Programs:
  [cenv](https://gitlab.com/pirivan/golearn/tree/master/cmd/mastergo/cenv) 

- [Oct 15, 2018] Exercises for Chapter 1 of _Mastering Go_ are complete. The
  chapter was a quick read as most of the content is not new; a couple of new
  things about logging but not much more. Wrote a simple program to provide the
  functionality of the three suggested exercises at the end of the
  chapter.<br/>Programs:
  [floats](https://gitlab.com/pirivan/golearn/tree/master/cmd/mastergo/floats)

- &#9745; [Aug 19, 2018] Exercises for Chapter 12 of _Go System Programming_ are
  complete. Wrote a _chat_ server that echoes every message sent by a client to
  any other client, that is, this is a multi-echo server. Clients can use the
  `/mute` and `/unmute` commands to avoid/resume echoing of their messages, and
  the `/quit` command to close the connections. The chat server uses a
  standalone _TCPServer_ server library which I wrote from scratch to abstract
  all the low-level TCP sockets details from the application. I put a special
  effort on writing good documentation for the chat and _TCPServer_
  programs.<br/>Programs:
  [chat](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/chat)
  [tcpserver](https://gitlab.com/pirivan/golearn/tree/master/internal/pkg/tcpserver)

- [Aug 06, 2018] Exercises for Chapter 11 of _Go System Programming_ are
  complete. Wrote a database program to manage city information from 
  [OpenWeatherMap](https://openweathermap.org). The implementation uses an
  `SQLite` database and supports all CRUD operations. Additionally, the database
  program exports data in text, CSV, and JSON formats. Wrote a weather web
  server that renders city information retrieved from the database program, as
  well as weather forecast data retrieved in real time using the REST API
  from `OpenWeatherMap`. Forecast rendering uses Golang HTML
  templates.<br/>Programs:
  [db](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/db)
  [weather](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/weather)
  
- [Jul 22, 2018] Exercises for Chapter 10 of _Go System Programming_ are
  complete. Modified the `pipeline` program to update a shared structure in a
  controlled way using a mutex. Modified the `socket` program to terminate if a
  timeout expires while waiting for new connections. <br/>Programs:
  [sharedmem](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/sharedmem),
  [timeout](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/timeout)

- [Jul 19, 2018] Exercises for Chapter 9 of _Go System Programming_ are
  complete. Wrote a program where two Go routines communicate via channels.
  Wrote a program that uses a pipeline of channels to search for a word on
  multiple files, and to report the number of ocurrences.<br/>Programs:
  [channels](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/channels),
  [pipeline](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/pipeline)

- [Jul 17, 2018] Exercises for Chapter 8 of _Go System Programming_ are
  complete. Updated the `ddGo` program to report progress when the SIGUSR1
  signal is received. Wrote a program where two Go routines communicate via
  pipes. Wrote a program where two Go routines communicate via a Unix socket;
  this program also accepts multiple client connections.<br/>Programs:
  [dd](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/dd),
  [pipe](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/pipe),
  [socket](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/socket)

- [Jul 12, 2018] Exercises for Chapter 7 of _Go System Programming_ are
  complete. Wrote a program to add line numbers to a file by writing the changes
  to a temporary file first, and then copying over the result over the original
  file. Wrote a program to remove a timestamp from lines on an input
  file.<br/>Programs:
  [insertLineNumber](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/insertLineNumber),
  [rmRegexp](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/rmRegexp)

- [Jul 11, 2018] Exercises for Chapter 6 of _Go System Programming_ are
  complete. Improved the `wc.go` program by fixing and reporting
  [bugs](https://github.com/PacktPublishing/Go-Systems-Programming/issues/3),
  and loading entire files into memory for processing.</br> Programs:
  [wc](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/wc)

- [Jul 4, 2018] Exercises for Chapter 5 of _Go System Programming_ are complete.
  Added command-line option to remove multiple files to the remove program, and
  modified the traverse program to list regular files only. Also, wrote two
  new programs, one with a minimum implementation of the `ls` command, with
  support for the `-a` and `-l` command line options, and another illustrating
  [How to Retrieve File Ownership Information in
  Golang](https://fosstel.com/post/golang/file-ownership/)</br> Programs:
  [rm](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/rm),
  [traverseFile](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/traverseFile),
  [ls](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/ls),
  [ugid](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/ugid)

- [Jun 24, 2018] Exercises for Chapter 4 of _Go System Programming_ are
  complete. Added search value and delete node capabilities to the binary tree
  program. Modified the hash program to prevent duplicate values in the hash
  table.</br> Programs:
  [tree](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/tree),
  [hash](https://gitlab.com/pirivan/golearn/tree/master/cmd/sysprog/hash)

- [Jun 12, 2018] Exercises for Chapter 3 of _Go System Programming_ are
  complete. The reflection program iterates recursively over a structure object,
  describing and modifying its fields.</br> Programs:
  [reflection](https://gitlab.com/pirivan/golearn/blob/master/cmd/sysprog/reflection),
  [ocurrences](https://gitlab.com/pirivan/golearn/blob/master/cmd/sysprog/occurrences)

- [Jun 03, 2018] Exercises for Chapter 2 of _Go System Programming_ are
  complete. Enhancements include dealing with `stdin` pipes and redirection,
  running unit tests, and [a Makefile for
  Golang](https://fosstel.com/post/golang/a-makefile-for-golang/).</br>
  Programs:
  [coordinates](https://gitlab.com/pirivan/golearn/blob/master/cmd/sysprog/coordinates),
  [minmax](https://gitlab.com/pirivan/golearn/blob/master/cmd/sysprog/minmax)

- [May 26, 2018] [Hello
  World!](https://gitlab.com/pirivan/golearn/blob/master/cmd/sysprog/hello)
  program complete, as per the [How to Write Go
  Code](https://golang.org/doc/code.html) tutorial. Code reorganized as
  suggested in the [Golang project layout
  guidelines](https://github.com/golang-standards/project-layout).

- &#9745; [May 19, 2018] [Tour of Go](https://tour.golang.org/) tutorial goal
  complete. My solutions to the exercises are available from the
  [snippets](https://gitlab.com/pirivan/golearn/snippets) section in Gitlab.
  
