---
title: Dataware
date: 2016-08-21T18:18:52-04:00
summary: "Data Science Learning Path (closed :thumbsup:)"
draft: false
showSummary: true
---

This project is my personal path to learning and practising on topics related to
Data Science, Machine Learning, and Deep Learning. The plan includes the
following goals:

- &#9745; Training on Machine Learning and Deep Learning. I completed the
  [Machine
  Learning](https://www.coursera.org/account/accomplishments/certificate/KA6Q3HPSL53M)
  course and the [Deep
  Learning](https://www.coursera.org/account/accomplishments/specialization/certificate/6K5DY2DZHJPE)
  specialization at [Coursera](https://www.coursera.org/). These courses are
  great and I highly recommend them, even if the former is a bit outdated. I
  gained a solid theoretical background of the math and "artistry" required to
  get the models working, and exposure to the basic Python stack for Data
  Science (Pandas, NumPy, Matplotlib, Scikit-Learn, TensorFlow, Keras).

- &#9745; Reading the [Python for Data
  Analysis](http://shop.oreilly.com/product/0636920050896.do) book and going
  through the exercises in detail. The goal is to solidify my expertise in using
  the Pandas-NumPy-Matplotlib stack.

- &#9758; Reading the [Hands-On Machine Learning with Scikit-Learn &
  TensorFlow](http://shop.oreilly.com/product/0636920052289.do) book and going
  through selected exercises in detail. The goal is to solidify my expertise on
  these two well known Python-based tools. This goal is my current focus.

- &#9758; Develop models of different kinds to tackle real-world data sets to
  infer valuable information. This effort will go from working with entry-level
  data sets such as [Titanic](https://www.kaggle.com/c/titanic) and
  [Iris](https://archive.ics.uci.edu/ml/datasets/iris) on my own (yes, I know,
  these data sets are too basic), to participating in bigger projects. Details
  will follow as things evolve, likely in parallel with my book-based learning
  efforts. On-going.


## Changelog

- &#9745; [Mar 25, 2019] Wrote [Titanic Machine Learning
  Models](https://nbviewer.jupyter.org/urls/gitlab.com/pirivan/dataware/raw/master/pydata/titanic/titanic-3.ipynb),
  a Jupyter notebook about the Titanic dataset. This is the last kernel in the
  series.  While completing the Titanic series I finished reading the _Python
  for Data Analysis_ book; objective achieved.

- [Feb 18, 2019] Wrote [Titanic Data
  Exploration](https://nbviewer.jupyter.org/urls/gitlab.com/pirivan/dataware/raw/master/pydata/titanic/titanic-2.ipynb),
  a Jupyter notebook about the Titanic dataset. This is the second notebook in
  the series.

- [Sep 28, 2018] Wrote [Titanic Data
  Cleanup](https://nbviewer.jupyter.org/urls/gitlab.com/pirivan/dataware/raw/master/pydata/titanic/titanic-1.ipynb),
  a Jupyter notebook about the Titanic dataset from the competition hosted at
  [Kaggle](https://www.kaggle.com/c/titanic). This is the first in a series of
  posts about the Titanic which will grow until I submit my model to the
  competition.

- [Sep 23, 2018] Finished reading chapter 12 of the _Python for Data Analysis_
  book. It is an interesting chapter on time series. My reading was lighter
  compared with previous chapters as I approached the chapter with the
  "reference guide" mindset. That is, I wanted to become aware of what is
  available more than in mastering the techniques in detail, which only comes
  with experience. I was impressed with the capabilities to handle dates and
  date periods which I had no idea they existed.

- [Sep 21, 2018] Finished reading chapters 8, 10, and 11 of the _Python for Data
  Analysis_ book. These chapters are massive to read as a story book, following
  every step in detail on my laptop. Half-way through my reading I realized
  that:
  
  1. I won't memorize all Numpy and pandas tricks by reading the book. Only
     experimenting with different datasets will provide me with useful
     expertise.
  
  2. Nevertheless, it is advisable to read the book following the details in my
     laptop. This helps me build a mental picture of the tool's capabilities
     which I wouldn't get easily otherwise.
     
  3. In the long term, the book must me treated as a reference guide whose
     content can be accessed at random to consult any individual topic as
     required.
  
    I skipped chapter 9 about matplotlib graphics for now as I am more
    interested in the core data wrangling with pandas first; I'll come back to it
    later.
  
- [Sep 13, 2018] Finished reading chapter seven of the _Python for Data
  Analysis_ book. The most interesting topics to me were the pandas' tools to
  handle missing data, discretization and binning, and vectorized string
  functions.

- [Sep 10, 2018] Finished reading chapter six of the _Python for Data Analysis_
  book. This is a small chapter on saving and loading data from Python, with a
  few topics new to me such as the HDF5 file format.

- [Sep 09, 2018] Finished reading chapter five of the _Python for Data
  Analysis_ book. This chapter is about panda's Series and Data Frames. I typed
  all exercises instead of just reading them. This is a better way to get
  concepts to sink in. There are no exercises to do, I'll leave them for the
  next chapters.

- [Sep 01, 2018] Completed work on chapter four of the _Python for Data
  Analysis_ book. This chapter is all about Numpy and a very brief intro to
  _matplotlib_. I wrote a program to exercise the topics discussed in this
  chapter. It generates and visualizes two different 2D random
  walks.</br>Programs:
  [random-walk](https://gitlab.com/pirivan/dataware/tree/master/pydata/random-walk)

- [Aug 24, 2018] Read the first three chapters of the _Python for Data
  Analysis_ book. Pretty easy reading as the last two chapters are just a review
  of the Python language.

- [Jul 31, 2018] Verified and updated my previous installation of IPython and
  Anaconda. Everything is up-to-date now.

- &#9745; [Feb 19, 2018] Completed the [Deep
  Learning](https://www.coursera.org/account/accomplishments/specialization/certificate/6K5DY2DZHJPE)
  specialization. This included the following five courses:  
  - Neural Networks and Deep Learning (four weeks)
  - Improving Deep Neural Networks: Hyperparameter tuning, Regularization and
    Optimization (three weeks)
  - Structuring Machine Learning Projects (two weeks)
  - Convolutional Neural Networks (four weeks)
  - Sequence Models (three weeks)

- &#9745; [Dec 23, 2017] Completed the [Machine
  Learning](https://www.coursera.org/account/accomplishments/certificate/KA6Q3HPSL53M)
  course. This course is spread over 11 weeks. It covers multiple topics,
  including linear and logistic regression on single and multiple variables,
  Support Vector Machines, unsupervised learning, neural networks,
  dimensionality reduction, anomaly detection, recommender systems, and several
  others. The course exercises are done in GNU Octave, which is interesting, but
  of not much use in data science anymore.
