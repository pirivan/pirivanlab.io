---
title: "Workstation logs"
description: "A collection of technical notes" 
---

This website is a collection on notes, mostly about random things that I do on my Linux and MacOS laptops.

{{< lead >}}
_Knowledge is possessed only by sharing; it is safeguarded by wisdom
and socialized by love. -- The Urantia Book, 48:7.28 (557.12)_
{{< /lead >}}

